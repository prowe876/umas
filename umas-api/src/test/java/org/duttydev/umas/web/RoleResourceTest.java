package org.duttydev.umas.web;

import org.duttydev.umas.domain.App;
import org.duttydev.umas.domain.Role;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URI;

/**
 * Created by prowe on 7/12/17.
 */

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RoleResourceTest {

    @Autowired
    private TestRestTemplate restTemplate;

    private App app;

    @Before
    public void setUp() {
        app = new App();
        app.setName("FB");

        URI appUrl = restTemplate.postForLocation("/api/apps",app);
        app = restTemplate.getForObject(appUrl,App.class);
    }

    @Test
    public void testCreateRoles() {
        Role newRole = new Role();
        newRole.setName("FB_ADMIN");
        newRole.setApp(app);

        URI roleUrl = restTemplate.postForLocation("/api/roles",newRole);
        Role createdRole = restTemplate.getForObject(roleUrl,Role.class);

        Assert.assertNotNull(createdRole);
        Assert.assertEquals(newRole.getName(),createdRole.getName());
        Assert.assertEquals(newRole.getApp(),createdRole.getApp());
    }

    @Test
    public void testUpdateRoles() {
        Role newRole = new Role();
        newRole.setName("FB_USER");
        newRole.setApp(app);

        URI roleUrl = restTemplate.postForLocation("/api/roles",newRole);
        Role createdRole = restTemplate.getForObject(roleUrl,Role.class);

        createdRole.setName("FB_USER2");
        restTemplate.postForLocation("/api/roles",createdRole);
        Role updatedRole = restTemplate.getForObject(roleUrl,Role.class);

        Assert.assertNotNull(updatedRole);
        Assert.assertNotEquals(updatedRole.getName(),newRole.getName());
        Assert.assertEquals(updatedRole.getId(),createdRole.getId());
        Assert.assertEquals(updatedRole.getApp(),createdRole.getApp());
    }
}
