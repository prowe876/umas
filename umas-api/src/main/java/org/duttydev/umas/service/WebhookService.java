package org.duttydev.umas.service;

import org.duttydev.umas.domain.Webhook;
import org.springframework.stereotype.Service;

/**
 * Created by prowe on 7/12/17.
 */
@Service
public class WebhookService extends BaseService<Webhook,Long> {
}
