package org.duttydev.umas.service;

import org.duttydev.umas.domain.App;
import org.duttydev.umas.repo.AppRepository;
import org.jose4j.jwk.RsaJsonWebKey;
import org.jose4j.jwk.RsaJwkGenerator;
import org.jose4j.lang.JoseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Optional;

/**
 * Created by prowe on 7/12/17.
 */
@Service
public class AppService extends BaseService<App,Long> {

    @Autowired
    private PasswordEncoder encoder;

    @Override
    public App saveEntity(App entity) {
        try {
            RsaJsonWebKey rsaJsonWebKey = RsaJwkGenerator.generateJwk(2048);
            rsaJsonWebKey.setKeyId(entity.getName()+"!=!"+Instant.now().getNano());
            entity.setKeyPair(rsaJsonWebKey);
        } catch(JoseException ex) {
            throw new RuntimeException(ex);
        }

        entity.setApiKey(encoder.encode(entity.getApiKey()));
        return super.saveEntity(entity);
    }

    public Optional<App> getByName(String name) {
        return ((AppRepository)repo).findByName(name);
    }
}
