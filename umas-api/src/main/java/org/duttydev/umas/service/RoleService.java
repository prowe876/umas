package org.duttydev.umas.service;

import org.duttydev.umas.domain.App;
import org.duttydev.umas.domain.Role;
import org.duttydev.umas.repo.RoleRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Created by prowe on 7/12/17.
 */
@Service
public class RoleService extends BaseService<Role,Long> {
    public Optional<Role> getByNameAndApp(String name,App app) {
        return ((RoleRepository)repo).findByNameAndApp(name,app);
    }

    public List<Role> getRolesByApp(App app) {
        return ((RoleRepository)repo).findByApp(app);
    }
}
