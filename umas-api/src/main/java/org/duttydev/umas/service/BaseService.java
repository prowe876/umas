package org.duttydev.umas.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Created by prowe on 7/12/17.
 */
public abstract class BaseService<E,ID extends Serializable> {

    @Autowired
    protected JpaRepository<E,ID> repo;

    public Page<E> getEntities(Pageable pageable) {
        return repo.findAll(pageable);
    }

    public List<E> getEntities(List<ID> ids) {
        return StreamSupport.stream(repo.findAll(ids).spliterator(),false).collect(Collectors.toList());
    }

    public Optional<E> getEntity(ID id) {
        return Optional.ofNullable(repo.findOne(id));
    }

    public E saveEntity(E entity) {
        return repo.saveAndFlush(entity);
    }

    public List<E> saveEntities(List<E> entities) {
        return StreamSupport.stream(repo.save(entities).spliterator(),false).collect(Collectors.toList());
    }

    public void deleteEntity(ID id) {
        repo.delete(id);
    }

    public void deleteEntities(List<ID> ids) {
        repo.deleteInBatch(getEntities(ids));
    }

}
