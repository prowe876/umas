package org.duttydev.umas.service;

import org.duttydev.umas.domain.App;
import org.duttydev.umas.domain.User;
import org.duttydev.umas.repo.UserRepository;
import org.jose4j.jwk.RsaJsonWebKey;
import org.jose4j.jwk.RsaJwkGenerator;
import org.jose4j.lang.JoseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Optional;

/**
 * Created by prowe on 7/12/17.
 */
@Service
public class UserService extends BaseService<User,Long> {

    @Autowired
    private PasswordEncoder encoder;

    @Override
    public User saveEntity(User entity) {
        entity.setPassword(encoder.encode(entity.getPassword()));
        return super.saveEntity(entity);
    }

    public Optional<User> getByNameAndApp(String name, App app) {
        return ((UserRepository)repo).findByNameAndApp(name,app);
    }
    public Optional<User> getByEmailAddressAndApp(String emailAddress,App app) {
        return ((UserRepository)repo).findByEmailAddressAndApp(emailAddress,app);

    }
}
