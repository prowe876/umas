package org.duttydev.umas.web;

import org.duttydev.umas.domain.Webhook;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by prowe on 7/12/17.
 */
@RestController
@RequestMapping(path="/api/webhooks",produces = MediaType.APPLICATION_JSON_VALUE)
public class WebhookResource extends BaseResource<Webhook,Long> {
}
