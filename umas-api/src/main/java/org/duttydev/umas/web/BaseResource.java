package org.duttydev.umas.web;

import org.duttydev.umas.domain.Identifiable;
import org.duttydev.umas.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.Serializable;
import java.net.URI;

/**
 * Created by prowe on 7/12/17.
 */
public abstract class BaseResource<E extends Identifiable,ID extends Serializable> {

    @Autowired
    private BaseService<E,ID> service;

    @GetMapping
    public ResponseEntity<Page<E>> getEntities(@RequestParam(defaultValue = "0") int page,@RequestParam(defaultValue = "10") int size) {
        return ResponseEntity.ok(service.getEntities(new PageRequest(page,size)));
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<E> getEntity(@PathVariable ID id) {
        return service.getEntity(id).map(e -> ResponseEntity.ok(e))
                .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    public ResponseEntity<Void> saveEntity(@RequestBody E entity) {
        ResponseEntity<Void> response = null;

        E result = service.saveEntity(entity);

        if(result == null) {
            response = ResponseEntity.noContent().build();
        } else {
            URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                    .path("/{id}")
                    .buildAndExpand(result.getId()).toUri();

            response = ResponseEntity.created(location).build();
        }

        return response;
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Void> deleteEntity(@PathVariable ID id) {
        service.deleteEntity(id);
        return ResponseEntity.ok().build();
    }
}
