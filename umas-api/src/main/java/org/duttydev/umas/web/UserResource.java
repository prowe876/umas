package org.duttydev.umas.web;

import org.duttydev.umas.domain.User;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by prowe on 7/12/17.
 */
@RestController
@RequestMapping(path="/api/users",produces = MediaType.APPLICATION_JSON_VALUE)
public class UserResource extends BaseResource<User,Long> {
}
