package org.duttydev.umas.web;

import org.duttydev.umas.auth.AuthenticationService;
import org.duttydev.umas.auth.UserAuthenticationResponse;
import org.duttydev.umas.auth.UserCredentials;
import org.duttydev.umas.domain.App;
import org.duttydev.umas.security.UmasAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

/**
 * Created by prowe on 7/13/17.
 */
@RestController
@RequestMapping(path="/auth",produces = MediaType.APPLICATION_JSON_VALUE)
public class AuthenticationResource {

    @Autowired
    private AuthenticationService authenticationService;

    @PostMapping(path = "/login")
    public ResponseEntity<UserAuthenticationResponse> login(@RequestBody UserCredentials credentials) {
        App app = ((UmasAuthentication)SecurityContextHolder.getContext().getAuthentication()).getApp();
        UserAuthenticationResponse response = authenticationService.login(app,credentials);
        return ResponseEntity.ok(response);
    }

}
