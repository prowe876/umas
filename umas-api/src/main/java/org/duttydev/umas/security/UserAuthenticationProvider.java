package org.duttydev.umas.security;

import org.duttydev.umas.auth.AuthenticationService;
import org.duttydev.umas.domain.App;
import org.duttydev.umas.domain.User;
import org.duttydev.umas.service.AppService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Optional;

/**
 * Created by pjrowe on 7/15/17.
 */
@Component
public class UserAuthenticationProvider implements AuthenticationProvider {
    private static final Logger LOG = LoggerFactory.getLogger(UserAuthenticationProvider.class);

    @Autowired
    private AppService appService;

    @Autowired
    private AuthenticationService authenticationService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        LOG.info("Authenticating in UserAuthenticationProvider: {}", authentication.getClass());

        if( StringUtils.isEmpty( ((UmasAuthentication)authentication).getAccessToken()) ) {
            throw new BadCredentialsException("No access token is provided");
        }

        String appName = authentication.getName();
        Optional<App> appOptional = appService.getByName(appName);

        String accessToken = (String) ((UmasAuthentication)authentication).getAccessToken();
        User user = authenticationService.validateAccessToken(appOptional.get(),accessToken);
        return new UmasAuthentication(appOptional.get(),user);
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(UmasAuthentication.class);
    }
}
