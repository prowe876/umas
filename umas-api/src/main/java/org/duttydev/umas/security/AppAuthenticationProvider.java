package org.duttydev.umas.security;

import org.duttydev.umas.domain.App;
import org.duttydev.umas.service.AppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Optional;

/**
 * Created by prowe on 7/13/17.
 */
@Component
public class AppAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private AppService appService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        Authentication result = null;

        String appName = authentication.getName();
        Optional<App> app = appService.getByName(appName);

        if(app.isPresent()) {
            result = verifyPassword(app.get(),authentication);
        }

        return result;
    }

    private Authentication verifyPassword(App app,Authentication authentication) {

        Authentication result = null;

        String credentials = (String)authentication.getCredentials();
        String encodedPassword = app.getApiKey();

        if(passwordEncoder.matches(credentials,encodedPassword)) {
            result =  new UmasAuthentication(app);
        }

        return result;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(UsernamePasswordAuthenticationToken.class);
    }
}
