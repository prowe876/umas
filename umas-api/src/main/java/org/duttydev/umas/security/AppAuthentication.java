package org.duttydev.umas.security;

import org.duttydev.umas.domain.App;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.Objects;

/**
 * Created by pjrowe on 7/16/17.
 */
public class AppAuthentication implements Authentication {

    private final App app;
    private String accessToken;

    public AppAuthentication(App app) {
        Objects.requireNonNull(app);
        this.app = app;
    }

    public App getApp() {
        return app;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public Object getCredentials() {
        return app.getApiKey();
    }

    @Override
    public Object getDetails() {
        return app;
    }

    @Override
    public Object getPrincipal() {
        return app.getName();
    }

    @Override
    public boolean isAuthenticated() {
        return false;
    }

    @Override
    public void setAuthenticated(boolean b) throws IllegalArgumentException {

    }

    @Override
    public String getName() {
        return app.getName();
    }
}
