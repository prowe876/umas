package org.duttydev.umas.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by prowe on 7/13/17.
 */
@Component
public class JWTAuthenticationFilter extends OncePerRequestFilter {

    private static final String ACCESS_TOKEN_KEY = "access_token";

    private final AuthenticationManager authenticationManager;

    @Autowired
    public JWTAuthenticationFilter(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
                                    FilterChain filterChain) throws ServletException, IOException {

        String accessToken = httpServletRequest.getParameter(ACCESS_TOKEN_KEY);

        if(!StringUtils.isEmpty(accessToken)) {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            ((UmasAuthentication)authentication).setAccessToken(accessToken);
        }
        filterChain.doFilter(httpServletRequest,httpServletResponse);

    }
}
