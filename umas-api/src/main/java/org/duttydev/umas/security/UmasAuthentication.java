package org.duttydev.umas.security;

import org.duttydev.umas.domain.App;
import org.duttydev.umas.domain.User;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created by pjrowe on 7/14/17.
 */
public class UmasAuthentication implements Authentication {

    private final App app;
    private final User user;
    private final boolean appAuthenticated;
    private final boolean userAuthenticated;
    private String accessToken;

    public UmasAuthentication(App app) {
        Objects.requireNonNull(app,"UmasAuthentication requires non null app");
        this.app = app;
        this.user = null;
        appAuthenticated = true;
        userAuthenticated = false;
    }

    public UmasAuthentication(App app,User user) {
        Objects.requireNonNull(app,"UmasAuthentication requires non null app");
        Objects.requireNonNull(user,"UmasAuthentication requires non null user");
        this.app = app;
        this.user = user;
        appAuthenticated = true;
        userAuthenticated = true;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public App getApp() {
        return app;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return user.getRoles().stream()
                .map(role -> new SimpleGrantedAuthority(role.getName()))
                .collect(Collectors.toList());
    }

    @Override
    public Object getCredentials() {
        return user == null ? app.getApiKey() : user.getPassword();
    }

    @Override
    public Object getDetails() {
        return user == null ? app : user;
    }

    @Override
    public Object getPrincipal() {
        return getName();
    }

    @Override
    public boolean isAuthenticated() {
        return appAuthenticated && userAuthenticated;
    }

    @Override
    public void setAuthenticated(boolean b) throws IllegalArgumentException {
        throw new UnsupportedOperationException("authentication cannot be set from outside");
    }

    @Override
    public String getName() {
        return user == null ? app.getName() : user.getEmailAddress();
    }
}
