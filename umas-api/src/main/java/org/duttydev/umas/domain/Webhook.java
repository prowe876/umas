package org.duttydev.umas.domain;

import org.hibernate.validator.constraints.Email;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

/**
 * Created by prowe on 7/12/17.
 */
@Entity
@Table(uniqueConstraints = @UniqueConstraint(name = "app_webhook",columnNames = {"app_id","url"}))
public class Webhook implements Identifiable{

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    @Email
    @Column(nullable = false)
    private String url;

    @ManyToOne
    @JoinColumn(name = "app_id")
    private App app;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public App getApp() {
        return app;
    }

    public void setApp(App app) {
        this.app = app;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Webhook webhook = (Webhook) o;

        if (id != null ? !id.equals(webhook.id) : webhook.id != null) {
            return false;
        }
        if (url != null ? !url.equals(webhook.url) : webhook.url != null) {
            return false;
        }
        return app != null ? app.equals(webhook.app) : webhook.app == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (url != null ? url.hashCode() : 0);
        result = 31 * result + (app != null ? app.hashCode() : 0);
        return result;
    }
}
