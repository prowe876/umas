package org.duttydev.umas.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.jose4j.jwk.RsaJsonWebKey;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by prowe on 7/12/17.
 */
@Entity
public class App implements Identifiable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Size(min = 1,max = 45)
    @Column(length = 45,nullable = false,unique = true)
    private String name;

    @NotNull
    @Size(min = 59,max = 72)
    @Column(length = 72, nullable = false)
    private String apiKey;

    @NotNull
    @Column(nullable = false)
    private Long jwtTTL = 30L;

    @JsonIgnore
    @Lob
    private RsaJsonWebKey keyPair;

    @JsonIgnore
    @OneToMany(mappedBy = "app")
    private Set<Webhook> webhooks = new HashSet<>();

    @JsonIgnore
    @OneToMany(mappedBy = "app")
    private Set<User> users = new HashSet<>(0);

    @JsonIgnore
    @OneToMany(mappedBy = "app")
    private Set<Role> roles = new HashSet<>(0);

    public RsaJsonWebKey getKeyPair() {
        return keyPair;
    }

    public void setKeyPair(RsaJsonWebKey keyPair) {
        this.keyPair = keyPair;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public Set<Webhook> getWebhooks() {
        return webhooks;
    }

    public void setWebhooks(Set<Webhook> webhooks) {
        this.webhooks = webhooks;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public Long getJwtTTL() {
        return jwtTTL;
    }

    public void setJwtTTL(Long jwtTTL) {
        this.jwtTTL = jwtTTL;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        App app = (App) o;

        if (id != null ? !id.equals(app.id) : app.id != null) {
            return false;
        }
        if (name != null ? !name.equals(app.name) : app.name != null) {
            return false;
        }
        if (apiKey != null ? !apiKey.equals(app.apiKey) : app.apiKey != null) {
            return false;
        }
        if (jwtTTL != null ? !jwtTTL.equals(app.jwtTTL) : app.jwtTTL != null) {
            return false;
        }
        if (webhooks != null ? !webhooks.equals(app.webhooks) : app.webhooks != null) {
            return false;
        }
        if (users != null ? !users.equals(app.users) : app.users != null) {
            return false;
        }
        return roles != null ? roles.equals(app.roles) : app.roles == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (apiKey != null ? apiKey.hashCode() : 0);
        result = 31 * result + (jwtTTL != null ? jwtTTL.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("App{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", apiKey='").append(apiKey).append('\'');
        sb.append(", jwtTTL=").append(jwtTTL);
        sb.append('}');
        return sb.toString();
    }
}
