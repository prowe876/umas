package org.duttydev.umas.domain;

/**
 * Created by prowe on 7/12/17.
 */
public interface Identifiable {
    public Long getId();
    public void setId(Long id);
}
