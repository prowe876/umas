package org.duttydev.umas.repo;

import org.duttydev.umas.domain.App;
import org.duttydev.umas.domain.Webhook;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by prowe on 7/12/17.
 */
@Repository
public interface WebhookRepository extends JpaRepository<Webhook,Long> {
    public List<Webhook> findByApp(App app);
}
