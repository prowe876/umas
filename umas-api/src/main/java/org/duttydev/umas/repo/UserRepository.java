package org.duttydev.umas.repo;

import org.duttydev.umas.domain.App;
import org.duttydev.umas.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Created by prowe on 7/12/17.
 */
@Repository
public interface UserRepository extends JpaRepository<User,Long> {
    public Optional<User> findByNameAndApp(String name,App app);
    public Optional<User> findByEmailAddressAndApp(String emailAddress,App app);
    public List<User> findByApp(App app);
}
