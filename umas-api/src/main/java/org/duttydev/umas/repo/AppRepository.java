package org.duttydev.umas.repo;

import org.duttydev.umas.domain.App;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Created by prowe on 7/12/17.
 */
@Repository
public interface AppRepository extends JpaRepository<App,Long> {
    public Optional<App> findByName(String name);
}
