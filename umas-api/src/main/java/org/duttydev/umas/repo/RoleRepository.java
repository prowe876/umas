package org.duttydev.umas.repo;

import org.duttydev.umas.domain.App;
import org.duttydev.umas.domain.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Created by prowe on 7/12/17.
 */
@Repository
public interface RoleRepository extends JpaRepository<Role,Long> {
    public Optional<Role> findByNameAndApp(String name,App app);
    public List<Role> findByApp(App app);
}
