package org.duttydev.umas.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Created by prowe on 7/12/17.
 */

@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

    @Value("${swagger.contact.name}")
    private String contactName;

    @Value("${swagger.contact.website}")
    private String contactWebsite;

    @Value("${swagger.contact.email}")
    private String contactEmail;

    @Bean
    public Docket apiDocumentation() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.ant("/api/**"))
                .build();
    }

    @Bean
    public ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("User Management & Authentication Service")
                .description("A simple api service that manages user information and provides "
                        + "authentication functionality. This service is able to support multiple applications and "
                        + "provides authentication using Json Web Tokens and Refresh Tokens")
                .contact(new Contact(contactName,contactWebsite,contactEmail))
                .license("MIT License")
                .licenseUrl("TODO")
                .termsOfServiceUrl("TODO")
                .build();
    }
}
