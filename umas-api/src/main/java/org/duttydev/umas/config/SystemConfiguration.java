package org.duttydev.umas.config;

import org.duttydev.umas.domain.App;
import org.duttydev.umas.domain.Role;
import org.duttydev.umas.domain.User;
import org.duttydev.umas.service.AppService;
import org.duttydev.umas.service.RoleService;
import org.duttydev.umas.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * Created by prowe on 7/12/17.
 */
@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "system")
public class SystemConfiguration  implements InitializingBean {
    private static final Logger LOGGER  = LoggerFactory.getLogger(SystemConfiguration.class);

    private static final String SYSTEM_APP_NAME = "SYSTEM";
    public static final List<String> SYSTEM_ROLES = Arrays.asList("SYS_ADMIN","SYS_USER");

    private String apiKey;
    private Map<String,String> jwt;
    private Map<String,String> refreshToken;
    private List<Map<String,Object>> users;

    @Autowired
    private AppService appService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private UserService userService;

    @Override
    public void afterPropertiesSet() throws Exception {

        App app = initializeApp();
        initializeRoles(app);
        initializeUsers(app);
    }

    private void initializeUsers(App app) {
        for (Map<String, Object> user : users) {
            String userName = (String) user.get("name");
            Optional<User> exisingUser = userService.getByNameAndApp(userName,app);

            if(!exisingUser.isPresent()) {
                User newUser = new User();
                newUser.setActive(true);
                newUser.setApp(app);
                newUser.setName(userName);
                newUser.setPassword((String)user.get("password"));
                newUser.setEmailAddress((String)user.get("email"));

                LinkedHashMap<String,String> roles = (LinkedHashMap<String,String>) user.get("roles");
                roles.forEach((index,roleName) -> {
                    newUser.getRoles().add(roleService.getByNameAndApp(roleName,app).get());
                });
                LOGGER.info("Created User: {}",userService.saveEntity(newUser));
            }

        }
    }

    private void initializeRoles(App app) {

        for(String roleName:SYSTEM_ROLES) {
            Optional<Role> role = roleService.getByNameAndApp(roleName,app);
            if(!role.isPresent()) {
                Role newRole = new Role();
                newRole.setName(roleName);
                newRole.setApp(app);
                roleService.saveEntity(newRole);
            }
        }

    }

    private App initializeApp() {
        Optional<App> existingSystemApp = appService.getByName(SYSTEM_APP_NAME);

        if(existingSystemApp.isPresent()) {
            return existingSystemApp.get();
        } else {
            App app = new App();
            app.setName(SYSTEM_APP_NAME);
            app.setApiKey(apiKey);
            app.setJwtTTL(Long.parseLong(jwt.getOrDefault("ttl","60")));
            return appService.saveEntity(app);
        }

    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public Map<String, String> getJwt() {
        return jwt;
    }

    public void setJwt(Map<String, String> jwt) {
        this.jwt = jwt;
    }

    public Map<String, String> getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(Map<String, String> refreshToken) {
        this.refreshToken = refreshToken;
    }

    public List<Map<String,Object>> getUsers() {
        return users;
    }

    public void setUsers(List<Map<String,Object>> users) {
        this.users = users;
    }
}
