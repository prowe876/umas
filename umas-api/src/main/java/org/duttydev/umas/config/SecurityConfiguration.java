package org.duttydev.umas.config;

import org.duttydev.umas.security.AppAuthenticationProvider;
import org.duttydev.umas.security.JWTAuthenticationFilter;
import org.duttydev.umas.security.UserAuthenticationProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

/**
 * Created by prowe on 7/13/17.
 */
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfiguration {

    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public BasicAuthenticationEntryPoint appEntryPoint() {
        BasicAuthenticationEntryPoint entryPoint = new BasicAuthenticationEntryPoint();
        entryPoint.setRealmName("umas");
        return entryPoint;
    }

    @Configuration
    @Order(1)
    public static class ApiWebSecurityConfigurationAdapter extends WebSecurityConfigurerAdapter {

        @Autowired
        private BasicAuthenticationEntryPoint appEntryPoint;

        @Autowired
        private AppAuthenticationProvider appAuthenticationProvider;

        @Autowired
        private UserAuthenticationProvider userAuthenticationProvider;

        @Autowired
        private JWTAuthenticationFilter authenticationFilter;

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http.authorizeRequests()
                    .antMatchers("/swagger/**").permitAll()
                    .antMatchers("/api/**").authenticated()
                    .antMatchers("/api/**").hasAnyAuthority("SYS_ADMIN","SYS_USER")
                    .antMatchers(HttpMethod.POST,"/api/**").hasAuthority("SYS_ADMIN")
                    .antMatchers(HttpMethod.DELETE,"/api/**").hasAuthority("SYS_ADMIN")
                    .and()
                    .httpBasic()
                    .authenticationEntryPoint(appEntryPoint)
                    .and()
                    .addFilterAfter(authenticationFilter, BasicAuthenticationFilter.class)
                    .csrf().disable();
        }

        @Autowired
        @Override
        protected void configure(AuthenticationManagerBuilder auth) throws Exception {
            auth.authenticationProvider(appAuthenticationProvider);
            auth.authenticationProvider(userAuthenticationProvider);
        }
    }

    @Configuration
    public static class AuthWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {

        @Autowired
        private BasicAuthenticationEntryPoint appEntryPoint;

        @Autowired
        private AppAuthenticationProvider authenticationProvider;

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http.authorizeRequests()
                    .antMatchers("/auth/**")
                    .authenticated()
                    .and()
                    .httpBasic()
                    .authenticationEntryPoint(appEntryPoint)
                    .and()
                    .csrf().disable();
        }

        @Autowired
        @Override
        protected void configure(AuthenticationManagerBuilder auth) throws Exception {
            auth.authenticationProvider(authenticationProvider);
        }
    }

}
