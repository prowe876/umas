package org.duttydev.umas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UmasApplication {

	public static void main(String[] args) {
		SpringApplication.run(UmasApplication.class, args);
	}
}
