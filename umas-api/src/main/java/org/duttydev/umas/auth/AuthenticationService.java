package org.duttydev.umas.auth;

import org.duttydev.umas.config.SystemConfiguration;
import org.duttydev.umas.domain.App;
import org.duttydev.umas.domain.Role;
import org.duttydev.umas.domain.User;
import org.duttydev.umas.service.UserService;
import org.jose4j.jwa.AlgorithmConstraints;
import org.jose4j.jwk.RsaJsonWebKey;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.MalformedClaimException;
import org.jose4j.jwt.consumer.InvalidJwtException;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Created by pjrowe on 7/14/17.
 */
@Service
public class AuthenticationService {
    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public UserAuthenticationResponse login(App app, UserCredentials credentials) throws BadCredentialsException {
        UserAuthenticationResponse response;

        // Get user & verify user
        Optional<User> userOptional = userService.getByEmailAddressAndApp(credentials.getEmailAddress(), app);
        if (userOptional.isPresent() &&
                passwordEncoder.matches(credentials.getPassword(), userOptional.get().getPassword())) {
            response = generateAccessToken(app.getKeyPair(), userOptional.get());
        } else {
            throw new BadCredentialsException("User or password is incorrect");
        }

        return response;
    }

    private UserAuthenticationResponse generateAccessToken(RsaJsonWebKey keyPair, User user) {

        try {
            RsaJsonWebKey rsaJsonWebKey = keyPair;

            JwtClaims claims = new JwtClaims();
            claims.setIssuer("umas");
            claims.setAudience(user.getApp().getName());
            claims.setExpirationTimeMinutesInTheFuture(user.getApp().getJwtTTL());
            claims.setGeneratedJwtId();
            claims.setIssuedAtToNow();
            claims.setNotBeforeMinutesInThePast(2);
            claims.setSubject(user.getName());
            claims.setClaim("email", user.getEmailAddress());
            List<String> roles = user.getRoles().stream().map(Role::getName).collect(Collectors.toList());
            claims.setStringListClaim("roles", roles);

            JsonWebSignature jws = new JsonWebSignature();
            jws.setPayload(claims.toJson());
            jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.RSA_USING_SHA256);
            jws.setKey(rsaJsonWebKey.getPrivateKey());
            jws.setKeyIdHeaderValue(rsaJsonWebKey.getKeyId());
            return new UserAuthenticationResponse(jws.getCompactSerialization(), "Bearer");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    public User validateAccessToken(App app, String accessToken) {

        JwtConsumer jwtConsumer = new JwtConsumerBuilder()
                .setRequireExpirationTime()
                .setAllowedClockSkewInSeconds(30)
                .setRequireSubject()
                .setExpectedIssuer("umas")
                .setExpectedAudience(app.getName())
                .setVerificationKey(app.getKeyPair().getKey())
                .setJwsAlgorithmConstraints(
                        new AlgorithmConstraints(AlgorithmConstraints.ConstraintType.WHITELIST,
                                AlgorithmIdentifiers.RSA_USING_SHA256))
                .build();

        User user;
        try {
            JwtClaims claims = jwtConsumer.processToClaims(accessToken);
            user = userService.getByNameAndApp(claims.getSubject(), app).get();
        } catch (MalformedClaimException e) {
            throw new RuntimeException("Getting username from jwt failed", e);
        } catch (InvalidJwtException ex) {
            throw new BadCredentialsException("Access token is invalid", ex);
        }

        return user;
    }


}
