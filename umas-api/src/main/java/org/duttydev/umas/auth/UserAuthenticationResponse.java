package org.duttydev.umas.auth;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by pjrowe on 7/14/17.
 */
public class UserAuthenticationResponse {

    @JsonProperty("access_token")
    private final String accessToken;

    @JsonProperty("token_type")
    private final String tokenType;

    public UserAuthenticationResponse(String accessToken, String tokenType) {
        this.accessToken = accessToken;
        this.tokenType = tokenType;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }
}
