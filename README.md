# User Management & Authentication Service #
----------------------------------------------

Side project for a simple user management and authentication service that supports multiple apps 
and manages those apps' users and roles. 

- umas-ui (SUSPENDED)
    - Angular 4
    - Semantic UI
    - Angular CLI
- umas-api (COMPLETED)
    - Spring Boot
    - Spring MVC
    - Spring Security
    - Swagger
    - Jose4J JWT
    - Maven

### Features
#### umas-api
- [x] Manage Apps, Users & Roles
- [x] Basic Authentication for Apps
- [x] Provide JWT on Login
- [x] Expose Functionality via REST

#### umas-ui
- [ ] Login Interface
- [ ] App Management Interface
- [ ] Role Management Interface
- [ ] User Management Interface
- [ ] Monitoring and Administration Interface

## License
--------------
### The MIT License
Copyright 2017 Peter-John Rowe

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
