/**
 * Created by pjrowe on 5/3/17.
 */
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TokenManagementComponent } from './components/token-management/token-management.component';
import { MonitoringComponent } from './components/monitoring/monitoring.component';
import { ClientManagementComponent } from './components/client-management/client-management.component';
import { GroupManagementComponent } from './components/group-management/group-management.component';
import { RoleManagementComponent } from './components/role-management/role-management.component';
import { UserManagementComponent } from './components/user-management/user-management.component';
import { LoginComponent } from './components/login/login.component';
import { NotAuthorizedComponent } from './components/not-authorized/not-authorized.component';
import { AuthenticationGuardGuard } from './guards/authentication-guard.guard';
import { AuthorizationGuardGuard } from './guards/authorization-guard.guard';

const routes: Routes = [
  { path: '', redirectTo: 'users', pathMatch: 'full', canActivate: [AuthenticationGuardGuard, AuthorizationGuardGuard]},
  { path: 'roles', component: RoleManagementComponent, canActivate: [AuthenticationGuardGuard, AuthorizationGuardGuard]},
  { path: 'clients', component: ClientManagementComponent, canActivate: [AuthenticationGuardGuard, AuthorizationGuardGuard]},
  { path: 'groups', component: GroupManagementComponent, canActivate: [AuthenticationGuardGuard, AuthorizationGuardGuard]},
  { path: 'users', component: UserManagementComponent, canActivate: [AuthenticationGuardGuard, AuthorizationGuardGuard]},
  { path: 'monitoring', component: MonitoringComponent, canActivate: [AuthenticationGuardGuard, AuthorizationGuardGuard]},
  { path: 'tokens', component: TokenManagementComponent, canActivate: [AuthenticationGuardGuard, AuthorizationGuardGuard]},
  { path: 'login', component: LoginComponent},
  { path: '404', component: NotAuthorizedComponent},
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
