import { Component, OnInit } from '@angular/core';
import { ClientManagementService } from '../../services/client-management.service';
import { ClientList } from './../../models/ClientList';
import { Client } from './../../models/client';

declare var $: any;

@Component({
  selector: 'app-client-management',
  templateUrl: './client-management.component.html',
  styleUrls: ['./client-management.component.css'],
})
export class ClientManagementComponent implements OnInit {

  clients: Client[];
  clientList: ClientList;
  loading = false;
  deleteMessage: string = '';

  constructor(private service: ClientManagementService) { }

  ngOnInit() {
    this.getClients();
  }

  private extractData(data: ClientList) {
    this.clientList = data;
    this.clients = data.content;
  }

  getClients() {
    this.service.getClients().subscribe(
      (data: ClientList) => this.extractData(data),
      error => console.log(error),
      () => console.log('Request Complete')
    );
  }

  deleteClient(id: number) {
    this.service.deleteClient(id).subscribe(
      function(result) {
        if (result) {
          this.getClients();
        }
      },
      error => console.log(error),
      () => console.log('Delete Client Request complete')
    );
    return false;
  }

  triggerDeleteClient(id: number) {
    // Get client from models
    const client = this.clients.find(c => c.id === id);

    // Set delete message
    this.deleteMessage = 'Are you sure you want to delete client ' + client.name + '?';

    // Trigger delete client modal
    $('.ui.basic.modal')
      .modal({
        onApprove: this.deleteClient(id)
      })
      .modal('show');
    // this.loading = true;

  }

}
