import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthenticationServiceService} from '../../services/authentication-service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  model: any = {};
  loading = false;
  loginFailed = false;
  errorMessage = '';

  constructor(private router: Router,
              private authenticationService: AuthenticationServiceService) {
  }

  ngOnInit() {
    this.authenticationService.logout();
  }

  login() {
    this.loading = true;
    this.authenticationService.login(this.model.username, this.model.password)
      .subscribe(
        response => {
          if (response.code === 200) {
            // login successful
            if (this.authenticationService.user.roles.indexOf('SYS_ADMIN') > -1) {
              this.router.navigate(['/clients']);
            } else {
              this.router.navigate(['/users']);
            }
          } else {
            // login failed
            this.errorMessage = response.message;
            this.loading = false;
            this.loginFailed = true;
          }
        }
      );
  }

}
