import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthenticationManagementComponent } from './authentication-management.component';

describe('AuthenticationManagementComponent', () => {
  let component: AuthenticationManagementComponent;
  let fixture: ComponentFixture<AuthenticationManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthenticationManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthenticationManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
