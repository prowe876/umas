/**
 * Created by pjrowe on 7/3/17.
 */
export class Client {

  loading = false;

  constructor(public id: number, public name: string, public emailAddress: string, public phoneNumber: string) {}
}
