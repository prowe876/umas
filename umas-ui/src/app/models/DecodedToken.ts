/**
 * Created by pjrowe on 5/17/17.
 */
export class DecodedToken {
  constructor(public username: string, public roles: object) {}
}
