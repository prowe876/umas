/**
 * Created by pjrowe on 5/16/17.
 */
export class AuthResponse {
  constructor(public code: number, public message: string) {}
}
