import {PaginatedList} from './PaginatedList';
import {Client} from './client';
/**
 * Created by pjrowe on 7/7/17.
 */
export class ClientList extends PaginatedList<Client> {}
