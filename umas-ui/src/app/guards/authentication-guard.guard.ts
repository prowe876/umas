import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import {AuthenticationServiceService} from '../services/authentication-service.service';

@Injectable()
export class AuthenticationGuardGuard implements CanActivate {

  constructor(private authenticationService: AuthenticationServiceService, private router: Router) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {

    if (this.authenticationService.isLoggedIn()) {
      // logged in so return true
      return true;
    } else {
      this.router.navigate(['/login']);
      return false;
    }

  }

}
