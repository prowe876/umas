import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import {AuthenticationServiceService} from '../services/authentication-service.service';

@Injectable()
export class AuthorizationGuardGuard implements CanActivate {

  constructor(private authenticationService: AuthenticationServiceService, private router: Router) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    if ( next.url.length !== 0 && next.url[0].path === 'clients' && this.authenticationService.user.roles.indexOf('SYS_ADMIN') < 0 ) {
      this.router.navigate(['/404']);
      return false;
    }

    return true;
  }
}
