import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';
import { AuthModule } from './auth.module';

import { AppComponent } from './app.component';
import { AuthenticationManagementComponent } from './components/authentication-management/authentication-management.component';
import { TokenManagementComponent } from './components/token-management/token-management.component';
import { MonitoringComponent } from './components/monitoring/monitoring.component';
import { ClientManagementComponent } from './components/client-management/client-management.component';
import { GroupManagementComponent } from './components/group-management/group-management.component';
import { RoleManagementComponent } from './components/role-management/role-management.component';
import { UserManagementComponent } from './components/user-management/user-management.component';
import { TokenManagementService } from './services/token-management.service';
import { UserManagementService } from './services/user-management.service';
import { RoleManagementService } from './services/role-management.service';
import { GroupManagementService } from './services/group-management.service';
import { ClientManagementService } from './services/client-management.service';
import { MonitoringService } from './services/monitoring.service';
import { HeaderComponent } from './components/core/header/header.component';
import { LoginComponent } from './components/login/login.component';
import { AuthenticationGuardGuard } from './guards/authentication-guard.guard';
import { AuthenticationServiceService } from './services/authentication-service.service';
import { AuthorizationGuardGuard } from './guards/authorization-guard.guard';
import { NotAuthorizedComponent } from './components/not-authorized/not-authorized.component';
import { ListComponent } from './components/core/list/list.component';
import { ListItemComponent } from './components/core/list-item/list-item.component';

@NgModule({
  declarations: [
    AppComponent,
    AuthenticationManagementComponent,
    TokenManagementComponent,
    MonitoringComponent,
    ClientManagementComponent,
    GroupManagementComponent,
    RoleManagementComponent,
    UserManagementComponent,
    HeaderComponent,
    LoginComponent,
    NotAuthorizedComponent,
    ListComponent,
    ListItemComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AuthModule,
    AppRoutingModule
  ],
  providers: [TokenManagementService, UserManagementService, RoleManagementService, GroupManagementService, ClientManagementService, MonitoringService, AuthenticationGuardGuard, AuthenticationServiceService, AuthorizationGuardGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
