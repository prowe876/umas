import {Injectable} from '@angular/core';
import {Http, Headers, RequestOptions, Response} from '@angular/http';
import {AuthResponse} from '../models/AuthResponse';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {tokenNotExpired, JwtHelper} from 'angular2-jwt';
import {User} from '../models/user';

@Injectable()
export class AuthenticationServiceService {
  public token: string;
  public decodedToken: object;
  public user: User;
  jwtHelper: JwtHelper = new JwtHelper();

  constructor(private http: Http) {
    this.token = localStorage.getItem('token');
    this.setUserDetails(this.token);
  }

  isLoggedIn() {
    return tokenNotExpired();
  }

  login(username: string, password: string): Observable<AuthResponse> {
    const clientId = 'xplode';
    const clientSecret = 'pl98nibsodbcs';

    const headers = new Headers(
      {
        'Authorization': 'Basic ' + btoa(clientId + ':' + clientSecret),
        'content-type': 'application/json'
      });

    const options = new RequestOptions({headers: headers});

    const body = JSON.stringify({username: username, password: password});

    return this.http.post('http://localhost:5050/api/auth/login', body, options)
      .map((response: Response) => {
        // login successful if there's a jwt token in the response
        const token = response.json() && response.json().access_token;
        if (token) {
          // set token property
          this.token = token;
          this.setUserDetails(token);

          // store username and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('token', token);
        }
        return new AuthResponse(200, 'Login successful');
      }).catch((error: Response) => {
        const errMsg = error.json();
        return Observable.of(new AuthResponse(errMsg.statusCode, errMsg.message));
      });
  }

  logout(): void {
    // clear token remove user from local storage to log user out
    this.token = null;
    this.decodedToken = null;
    this.user = null;
    localStorage.removeItem('token');
  }

  private setUserDetails(token: string) {
    if (token != null) {
      this.decodedToken = this.jwtHelper.decodeToken(token);
      const username: string = this.decodedToken['sub'];
      const roles: string[] = this.decodedToken['roles'];
      this.user = new User(username, roles);
    }
  }

}
