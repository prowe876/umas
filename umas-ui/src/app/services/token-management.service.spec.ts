import { TestBed, inject } from '@angular/core/testing';

import { TokenManagementService } from './token-management.service';

describe('TokenManagementService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TokenManagementService]
    });
  });

  it('should ...', inject([TokenManagementService], (service: TokenManagementService) => {
    expect(service).toBeTruthy();
  }));
});
