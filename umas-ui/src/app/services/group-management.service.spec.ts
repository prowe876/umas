import { TestBed, inject } from '@angular/core/testing';

import { GroupManagementService } from './group-management.service';

describe('GroupManagementService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GroupManagementService]
    });
  });

  it('should ...', inject([GroupManagementService], (service: GroupManagementService) => {
    expect(service).toBeTruthy();
  }));
});
