import { Injectable } from '@angular/core';
import { AuthHttp } from 'angular2-jwt';
import { Headers, RequestOptions, Response } from '@angular/http';
import {Observable} from 'rxjs/Observable';
import { ClientList } from './../models/ClientList';

@Injectable()
export class ClientManagementService {

  constructor(public authHttp: AuthHttp) { }

  getClients(): Observable<ClientList> {

    const clientId = 'xplode';
    const clientSecret = 'pl98nibsodbcs';

    const headers = new Headers(
      {
        'Authorization': 'Basic ' + btoa(clientId + ':' + clientSecret),
        'content-type': 'application/json'
      });

    const options = new RequestOptions({headers: headers});

    return this.authHttp.get('http://localhost:5050/api/clients', options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  deleteClient(id: number): Observable<void> {
    const clientId = 'xplode';
    const clientSecret = 'pl98nibsodbcs';

    const headers = new Headers(
      {
        'Authorization': 'Basic ' + btoa(clientId + ':' + clientSecret),
        'content-type': 'application/json'
      });

    const options = new RequestOptions({headers: headers});

    return this.authHttp.delete('http://localhost:5050/api/clients/' + id, options)
      .map(response => response.status === 200)
      .catch(this.handleError);
  }

  private extractData(res: Response): ClientList {
    const body = res.json();
    return body || { };
  }

  private handleError (error: Response | any) {
    // In a real world app, you might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

}
