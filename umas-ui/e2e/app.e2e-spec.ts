import { UmasUiPage } from './app.po';

describe('umas-ui App', () => {
  let page: UmasUiPage;

  beforeEach(() => {
    page = new UmasUiPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
